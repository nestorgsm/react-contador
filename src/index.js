import ReactDOM from 'react-dom'
import CounterApp from './CounterApp';
import FirstApp from './FirstApp';
import './index.css'

const divRoot = document.querySelector('#root');

// ReactDOM.render(<FirstApp saludo='Hola Nes' />, divRoot);
ReactDOM.render(<CounterApp valor={0} />, divRoot);