import PropTypes from 'prop-types';

const FirstApp = ( {saludo, subtitulo} ) => {

    return (
        <>
            {/* <pre>{JSON.stringify(obj, null, 3)}</pre> */}
            <h1> {saludo} </h1>
            <p>{subtitulo}</p>
        </>
    );

}

FirstApp.propTypes = {
    saludo: PropTypes.string.isRequired
}

FirstApp.defaultProps = {
    subtitulo: 'Soy un subtitulo'
}


export default FirstApp