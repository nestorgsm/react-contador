import PropTypes from 'prop-types';
import { useState } from 'react';

const CounterApp = ( {valor = 10} ) => {

    const [ counter, setCounter ] = useState(valor);
    // console.log(first)

    const eventoAdd = () => {
        setCounter( counter + 1)
        
    }

    const eventoReset = () => setCounter(valor) 

    const eventoSubtract = () => setCounter( counter - 1)

    return (
        <>
            <h1>CounterApp</h1>
            <h2>{counter}</h2>
            <button onClick={ eventoAdd }>+1</button>
            <button onClick={ eventoReset }>Reset</button>
            <button onClick={ eventoSubtract }>-1</button>
        </>
    )
}

CounterApp.propTypes = {
    valor: PropTypes.number
}

export default CounterApp